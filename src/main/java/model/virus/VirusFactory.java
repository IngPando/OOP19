package model.virus;

/**
 * Interface that models the virus creator.
 */
public interface VirusFactory {


    /**
     * @return
     *      The Virus
     */
    Virus createVirus();

}
